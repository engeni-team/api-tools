# Engeni API Tools

[!(http://engeni.com/emails/signature/logo.png)](https://engeni.com)

Engeni API Tools is a set of classes that are ment to unify the API developement of microservices.

- RESTful URLs and actions
- Result filtering, sorting & searching
- Limiting which fields are returned by the API
- Updates & creation return a resource representation
- Context and API Context handling
- And much more...

Please note that you need an Engeni API Key to access all Engeni API resources. Please, get in contact with Engeni at: info@engeni.com.

## Installation

Install composer first. Then:

```sh
composer require engeni/api-tools
```

### Api client service provider

This service provider can be used to load an api client instance as a service for laravel or lumen. It requires `engeni/api-client` to be installed

You can create your own service provider and extend `/Providers/ApiClientServiceProvider.php` or use it directly, remember to add the service on the service provider list for lumen or laravel app.

### TokenAuth middleware

Use this middleware to authenticate the user for a lumen/laravel api protected by an oauth2 server (Passport) `/Http/Middleware/TokenAuth.php`.

You can use it directly or extend from it and add or override functionality.

## Example of use

In order to have the Engeni API capabilities you have to use use trair RestfulControllerTrait.

### Example 1

Only using the trait

```php
use Illuminate\Routing\Controller as BaseController;
use Engeni\ApiTools\Traits\RestfulControllerTrait;

class UserController extends BaseController
{
    use RestfulControllerTrait;
}
```

### Example 2

Extending a class that uses the trait.
Then $model has to be defined to do the magic (index (get /), show (get for id), store, update, destroy)

```php
namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use Engeni\ApiTools\Traits\RestfulControllerTrait;

class Controller extends BaseController
{
    use RestfulControllerTrait;
    //
}
```

Then:

```php
namespace App\Http\Controllers;

class UserController extends Controller
{
    // Trait related lines
    protected $model = 'App\Models\User'; // <- optional

    // optional
    protected $validationRules = ['username' => 'required',
                                  'name' => 'required',
                                  'password' => 'required'];
}
```

If you don't define $model then the class parses the Controller classname to get it.
I.e: From UserController will get User and define the model as App\Models\User

Enjoy.
The Engeni Team.
<https://engeni.com>