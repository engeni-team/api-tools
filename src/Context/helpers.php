<?php

use Engeni\ApiTools\Context\AppContextRegister;

if (! function_exists('app_context')) {
    function app_context(): AppContextRegister
    {
        return app(AppContextRegister::class);
    }
}
