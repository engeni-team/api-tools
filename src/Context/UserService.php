<?php

namespace Engeni\ApiTools\Context;

use Engeni\ApiClient\Resource\CXM\Auth;
use Illuminate\Support\Facades\Log;
use Throwable;

class UserService
{
    /**
     * @throws Throwable
     */
    public function getUserFromCxm(string $bearerToken): ?Auth
    {
        try {
            return Auth::getByToken($bearerToken) ?? null;
        } catch (Throwable $e) {
            Log::error($e);
            throw $e;
        }
    }
}
