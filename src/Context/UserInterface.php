<?php

namespace Engeni\ApiTools\Context;

interface UserInterface
{
    public function forceFill(array $attributes);

    public function isCxmUser(): bool;
}
