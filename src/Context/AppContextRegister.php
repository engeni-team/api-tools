<?php

namespace Engeni\ApiTools\Context;

use Illuminate\Support\Traits\ForwardsCalls;

/**
 * @method UserInterface getUser()
 * @method bool hasUser()
 */
class AppContextRegister
{
    use ForwardsCalls;

    public AppContext $context;

    public function __construct(?AppContext $context = null)
    {
        if (is_null($context)) {
            $this->registerContext(app(AppContext::class));
        } else {
            $this->registerContext($context);
        }
    }

    public function registerContext(AppContext $context): AppContext
    {
        return $this->context = $context;
    }

    public function context(): AppContext
    {
        return $this->context;
    }

    public function __call($method, $parameters)
    {
        return $this->forwardCallTo($this->context(), $method, $parameters);
    }

    public function __wakeup()
    {
        return app()->singleton(AppContextRegister::class, fn ($app) => $this);
    }

    public function registerCLIContext(): AppContext
    {
        $context = app(AppContextGenerator::class)->createContextForCLIUser();

        return $this->registerContext($context);
    }

    public function __sleep()
    {
        return ['context'];
    }
}
