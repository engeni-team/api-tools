<?php

namespace Engeni\ApiTools\Context;

use Exception;

class AppContext
{
    protected ?UserInterface $user = null;

    public function hasUser(): bool
    {
        return isset($this->user);
    }

    public function setUser(UserInterface $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @throws Exception
     */
    public function getUser(): ?UserInterface
    {
        if ($this->hasUser() === false) {
            throw new Exception('User is not set in AppContext.', 422);
        }

        return $this->user;
    }
}
