<?php

namespace Engeni\ApiTools\Context\Middleware;

use Closure;
use Engeni\ApiTools\Context\AppContextGenerator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Throwable;

class ResolveContext
{
    public function __construct(protected AppContextGenerator $appContextGenerator) {}

    /**
     * Handle an incoming request.
     *
     * @throws Throwable
     */
    public function handle(Request $request, Closure $next): mixed
    {
        $context = $this->appContextGenerator->fromRequest($request);

        if ($context && $context->hasUser()) {
            Auth::setUser($context->getUser());

            return $next($request);
        }

        return response('Unauthorized.', 401);
    }
}
