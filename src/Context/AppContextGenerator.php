<?php

namespace Engeni\ApiTools\Context;

use Engeni\ApiClient\Resource\CXM\Auth;
use Engeni\ApiClient\Resource\CXM\User;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Log;
use stdClass;
use Throwable;

class AppContextGenerator
{
    protected array $userAttributesForContext = [
        'id',
        'name',
        'email',
        'active',
        'role_id',
        'language_id',
        'current_account_id',
        'current_order_id',
        'last_login',
        'created_at',
        'updated_at',
    ];

    public function __construct(protected UserService $userService, protected AppContextRegister $register) {}

    /**
     * @throws Throwable
     */
    public function fromRequest(Request $request): ?AppContext
    {
        try {
            /** @var AppContext $context */
            if ($user = auth()->user()) {
                /** @var UserInterface $user */
                $context = $this->createContextFromAuthUser($user);

                return $this->register->registerContext($context);
            }

            if ($userFromHeader = $request->header('user-info')) {
                /** @var string $userFromHeader */
                /** @var stdClass $userDecoded */
                $userDecoded = json_decode($userFromHeader);
                $context = $this->createContextFromUserInfoHeader($userDecoded);

                return $this->register->registerContext($context);
            }

            if ($bearerToken = $request->bearerToken()) {
                /** @var Auth|User $userFromCXM */
                $userFromCXM = $this->userService->getUserFromCxm($bearerToken);
                $context = $this->createContextFromCXMUser($userFromCXM);

                return $this->register->registerContext($context);
            }

            if ($request->header('X-Api-Token') == config('engeni.api_token')) {
                $context = $this->createContextForCLIUser();

                return $this->register->registerContext($context);
            }
        } catch (Throwable $exception) {
            Log::error('Error while trying to generate a context: '.$exception->getMessage());
        }

        return null;
    }

    public function createContextForCLIUser(): AppContext
    {
        $user = $this->getMockedCLIUser();

        return $this->createContextFromUserInfoHeader($user);
    }

    public function getMockedCLIUser(): stdClass
    {
        return (object) ['id' => 1, 'name' => 'Command line interface', 'email' => 'cli@engeni.com', 'active' => 1, 'role_id' => 1, 'language_id' => 1245];
    }

    public function createContextFromAuthUser(UserInterface $userFromAuth): AppContext
    {
        /** @var AppContext $context */
        $context = app(AppContext::class);
        $user = app(UserInterface::class);
        $attributes = Arr::only($userFromAuth->toArray(), $this->userAttributesForContext);
        $user->forceFill($attributes);

        return $context->setUser($userFromAuth);
    }

    public function createContextFromUserInfoHeader(stdClass $userFromHeader): AppContext
    {
        /** @var AppContext $context */
        /** @var UserInterface $user */
        $context = app(AppContext::class);
        $user = app(UserInterface::class);
        $attributes = Arr::only((array) $userFromHeader, $this->userAttributesForContext);
        $user->forceFill($attributes);

        return $context->setUser($user);
    }

    public function createContextFromCXMUser(Auth|User $userFromCXM): AppContext
    {
        /** @var AppContext $context */
        /** @var UserInterface $user */
        $context = app(AppContext::class);
        $user = app(UserInterface::class);
        $attributes = Arr::only($userFromCXM->toArray(), $this->userAttributesForContext);
        $user->forceFill($attributes);

        return $context->setUser($user);
    }
}
