<?php

namespace Engeni\ApiTools\Context;

use Illuminate\Support\ServiceProvider;

class AppContextServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $this->app->singleton(AppContextRegister::class, fn ($app) => new AppContextRegister);
    }

    public function register()
    {
        require_once __DIR__.'/helpers.php';
    }
}
