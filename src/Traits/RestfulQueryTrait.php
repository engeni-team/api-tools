<?php

namespace Engeni\ApiTools\Traits;

use Engeni\ApiTools\Helpers\RestfulResponse;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Http\Request;

trait RestfulQueryTrait
{
    protected function getModel()
    {
        return $this->model;
    }

    /*
     * $sort may be: sort=-name,date_created
     * This returns an array of:
     * sortOptions['name DESC', 'date_created ASC']
     */
    protected function scopeOfSort(&$query, Request $request)
    {
        if ($request->get('sort')) {
            $sortFields = explode(',', $request->get('sort'));
            foreach ($sortFields as $sortField) {
                $sortField = trim($sortField);
                if (str_contains($sortField, '.')) {
                    $embedSort = explode('.', $sortField);
                    array_pop($embedSort);
                    if (count($embedSort) == 1) {
                        $embedSort = str_replace('-', '', $embedSort[0]);
                        $model = $this->getModel();
                        if ($this->validateEmbedRelation($model, $embedSort)) {
                            $modelInstance = (new $model);
                            $parentTableName = $modelInstance->getTable();
                            $relation = $modelInstance->$embedSort();
                            if (! ($relation instanceof HasMany)) {
                                if ($relation instanceof BelongsTo) {
                                    $relatedKey = $relation->getQualifiedOwnerKeyName();
                                    $parentKey = $relation->getQualifiedForeignKey();
                                } else {
                                    $relatedKey = $relation->getExistenceCompareKey();
                                    $parentKey = $relation->getQualifiedParentKeyName();
                                }
                                $relatedTable = $relation->getRelated()->getTable();
                                $query->join($relatedTable, $relatedKey, '=', $parentKey)->select(["{$parentTableName}.*"]);
                                $sortField = str_replace($embedSort, $relatedTable, $sortField);
                            } else {
                                $sortField = null;
                            }
                        }
                    }
                }
                if ($sortField) {
                    if (preg_match('/-(.*)/', $sortField, $pregMatchResult)) {
                        if ($query) {
                            $query->orderBy($pregMatchResult[1], 'DESC');
                        }
                    } elseif ($query) {
                        $query->orderBy($sortField, 'ASC');
                    }
                }
            }
        }

        return $query;
    }

    protected function scopeOfEmbededRelations(&$query, Request $request, $model = null)
    {
        $model = $model ?: $this->getModel();
        if (! $model) {
            dd('scopeOfEmbededRelations: Please define model');
        }
        if ($request->has('embed') && ($embededRelations = $this->parseEmbedQuery($request->get('embed'))) != null) {
            foreach ($embededRelations as $relation) {
                if ($this->validateEmbedRelation($model, $relation)) {
                    $query->with($relation);
                }
            }
        }

        return $query;
    }

    protected function getModelWithEmbedRelations($model, Request $request)
    {
        if ($request->has('embed') && ($embededRelations = $this->parseEmbedQuery($request->get('embed'))) != null) {
            foreach ($embededRelations as $relation) {
                if ($this->validateEmbedRelation($model, $relation)) {
                    $model->load($relation);
                }
            }
        }

        return $model;
    }

    protected function scopeOfFields(&$query, Request $request, $model = null)
    {
        $model = $model ?: $this->getModel();
        if (! $model) {
            dd('scopeOfFields: Please define model');
        }
        if ($request->has('fields')) {
            $query->select(explode(',', $request->get('fields')));
        }

        return $query;
    }

    protected function getList($query, Request $request)
    {
        $results = [];
        try {
            // Allow embeded relations
            $this->scopeOfEmbededRelations($query, $request);

            // Sorting options
            $this->scopeOfSort($query, $request);

            // Allow selecting a few fields
            $this->scopeOfFields($query, $request);

            // Pagination
            if ($request->has('no_pagination') || $request->has('limit')) {
                if ($request->has('limit')) {
                    $query->take((int) $request->get('limit'));
                }
                $results = $query->get();
            } else {
                $results = $query->paginate($request->has('per_page') ? (int) $request->get('per_page') : 20);
            }
        } catch (Exception $e) {
            return RestfulResponse::sendClientError($e->getMessage());
        }

        return $results;
    }

    protected function parseEmbedQuery($embed = '')
    {
        $EmbeddedRelations = explode(',', (string) $embed);

        return $embed != '' ? $EmbeddedRelations : null;
    }

    protected function validateEmbedRelation($model, $relation)
    {
        // $relation can be as follows:
        // Model User
        // Relation account
        // Or relation account.partner
        // Currently we only can validate the first level of relationship.
        // So un account.partner, only account is validated
        $relation = explode('.', (string) $relation);

        return method_exists($model, $relation[0]);
    }
}
