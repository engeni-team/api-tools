<?php

namespace Engeni\ApiTools\Traits;

trait Accountable
{
    public static function bootAccountable()
    {
        static::addGlobalScope(new AccountableScope);
    }

    public function setAccountId()
    {
        if (app_context()->hasAccount()) {
            $this->account_id = app_context()->getAccount()->id;
        }
    }

    public function hasAccountId()
    {
        return true;
    }
}
