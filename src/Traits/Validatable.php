<?php

namespace Engeni\ApiTools\Traits;

use Engeni\ApiTools\Exceptions\RecordInvalidException;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Validator as BaseValidator;

/**
 * Validatable is a trait applied to Model objects.
 * It allows to validate a model before saving it to the database.
 */
trait Validatable
{
    // Define your rules in the Model class
    protected array $rules = [];

    public array $errors = [];

    protected BaseValidator $validation;

    public function addError($attribute, $msg): array
    {
        $this->errors[$attribute] = $msg;

        return $this->errors;
    }

    public function addErrors(array $errors): self
    {
        foreach ($errors as $key => $value) {
            $this->addError($key, $value);
        }

        return $this;
    }

    public function errors(): array
    {
        return $this->errors;
    }

    public function getErrorMessages(): string
    {
        $errorMessages = collect($this->errors())->flatMap(fn ($error) => $error);

        return $errorMessages->implode(' ');
    }

    public function getValidation(): BaseValidator
    {
        return $this->validation;
    }

    public function setValidation($validation)
    {
        $this->validation = $validation;

        return $this;
    }

    public function isInvalid(): bool
    {
        return ! $this->isValid();
    }

    public function isValid(): bool
    {
        return empty($this->errors);
    }

    public function validate(): self
    {
        if (method_exists($this, 'beforeValidation')) {
            $this->beforeValidation();
        }

        $this->setValidation(Validator::make(
            $this->attributesToArray(),
            $this->getValidationRules(),
            $this->errorMessages ?? []
        ));

        $this->errors = [
            ...$this->errors,
            ...$this->getValidation()->errors()->messages(),
        ];

        return $this;
    }

    public function save(array $options = [])
    {
        $this->validate();

        if ($this->isValid()) {
            return parent::save($options);
        }

        return false;
    }

    public function saveOrFail(array $options = [])
    {
        $this->validate();
        if ($this->isValid()) {
            return parent::saveOrFail($options);
        }

        throw new RecordInvalidException($this);
    }

    public function getValidationRules(): array
    {
        return $this->rules;
    }

    public function setValidationRules(array $rules)
    {
        $this->rules = $rules;

        return $this;
    }

    public function addValidationRules(array $rules)
    {
        $this->setValidationRules([
            ...$this->getValidationRules(),
            ...$rules,
        ]);

        return $this;
    }
}
