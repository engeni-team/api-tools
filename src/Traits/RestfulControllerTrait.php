<?php

namespace Engeni\ApiTools\Traits;

use Engeni\ApiTools\Exceptions\AppValidationException;
use Engeni\ApiTools\Helpers\RestfulResponse;
use Engeni\ApiTools\Resources\PaginateResourceCollection;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

trait RestfulControllerTrait
{
    use RestfulQueryTrait;

    // Don't forget to define these in your class!!!
    // protected $rules = [];
    // protected $model = null;
    protected $params = [];

    /**
     * Optional. if defined the controller will use the resource as returned value.
     * If false, the RestfulControllerTrait will avoid inject a JSON Resource
     * if null, it will look for the JSON Resource in the App\Http\Resources\ folder.
     *
     * @var mixed
     */
    protected $jsonResource = null;

    // DEPRECATED!!!
    public function getModel()
    {
        return $this->getModelClass();
    }

    /**
     * Gets the model class name from the Controller class name.
     *
     * @return string
     */
    public function getModelClass()
    {
        if (! isset($this->model) || ! $this->model) {
            preg_match('/([a-zA-Z]*)Controller/', class_basename($this), $matches);
            $class = $matches[1];
            $namespace = str_replace('\Http', '', $this::class);
            $namespace = str_replace($class.'Controller', '', $namespace);
            $namespace = str_replace('\Api', '', $namespace);
            $namespace = str_replace('Controllers', 'Models', $namespace);
            $this->model = ($namespace.$class);
        }

        return $this->model;
    }

    /*
     * Instead of returning the model directly, we prefer to return
     * an API JSON resource.
     */
    public function getResource($model)
    {
        if ($this->getJsonResourceClass()) {
            $resource = $this->getJsonResourceClass();

            return new $resource($model);
        }

        return $model;
    }

    public function getResourceCollection($collection)
    {
        if ($this->getJsonResourceClass()) {
            $resource = $this->getJsonResourceClass();
            if ($collection instanceof \Illuminate\Pagination\AbstractPaginator) {
                $resourceCollection = new PaginateResourceCollection($collection);
                $resourceCollection->setCollects($resource);

                return $resourceCollection;
            } else {
                return $resource::collection($collection);
            }
        }

        return $collection;
    }

    private function getJsonResourceClass()
    {
        if ($this->jsonResource !== false) {
            if (class_exists($this->jsonResource)) {
                return $this->jsonResource;
            } else {
                $class = $this::class;
                $class = str_replace('Controllers', 'Resources', $class);
                $class = preg_replace('/(.*)Controller$/', '$1Resource', $class);

                return class_exists($class) ? $class : null;
            }
        }

        return null;
    }

    public function queryFilter($query, $request)
    {
        return $query;
    }

    public function setParams($params = [])
    {
        $this->params = $params;
    }

    public function request()
    {
        return app('request');
    }

    public function getTableColumns()
    {
        $model = $this->getModelClass();
        $model = (new $model);

        return $model->getConnection()->getSchemaBuilder()->getColumnListing($model->getTable());
    }

    public function getFilterableColumns()
    {
        return $this->getTableColumns();
    }

    public function columnsFilter($query, $params)
    {
        $attributesToFilter = array_intersect($this->getFilterableColumns(), array_keys((array) $params));
        foreach ($attributesToFilter as $attribute) {
            $values = $params[$attribute];
            if (is_array($values)) {
                $query = $query->whereIn($attribute, $values);
            } else {
                if ($values === 'true') {
                    $values = true;
                }
                if ($values === 'false') {
                    $values = false;
                }

                $query = $query->where($attribute, $values);
            }
        }

        return $query;
    }

    /**
     * Execute the authorize() method from the controller if possible.
     *
     * @param  mixed  $model  The instanciated model or the model class
     * @return null;
     */
    public function tryToAuthorize(string $ability, mixed $model = null)
    {
        if (Gate::getPolicyFor($this->getModelClass())) {
            if ($model) {
                $this->authorize($ability, $model);
            } else {
                $this->authorize($ability, $this->getModelClass());
            }
        }
    }

    /*
     * Typical controller methods
     */
    public function index(Request $request)
    {
        $model = $this->getModelClass();
        $query = $model::query();
        $query = $this->columnsFilter($query, $request->all());
        $query = $this->queryFilter($query, $request);
        $results = $this->getList($query, $request);

        return RestfulResponse::sendList($this->getResourceCollection($results));
    }

    public function show(Request $request, $id)
    {
        $model = $this->getModelClass();
        $query = $model::query();
        $query = $this->columnsFilter($query, $this->params);
        $this->scopeOfEmbededRelations($query, $request);
        // Allow selecting a few fields
        $this->scopeOfFields($query, $request);
        $data = $query->findOrFail($id);

        return RestfulResponse::sendShow($this->getResource($data));
    }

    public function store(Request $request)
    {
        $model = $this->getModelClass();
        $model = new $model(array_merge($this->params, $request->all()));
        if (! $model->save()) {
            throw new AppValidationException(
                __(
                    'errors.entity_not_saved',
                    [
                        'entity' => (new \ReflectionClass($this->getModelClass()))->getShortName(),
                    ]
                ),
                0,
                $model->errors()
            );
        }
        $model = $this->getModelWithEmbedRelations($model, $request);
        $this->afterCreate($model);

        return RestfulResponse::sendCreated($this->getResource($model));
    }

    public function update(Request $request, $id)
    {
        $model = $this->getModelClass();
        $query = $model::query();
        $query = $this->columnsFilter($query, $this->params);
        $model = $query->findOrFail($id);
        $model = $model->fill($request->all());
        if (! $model->save()) {
            throw new AppValidationException(
                __(
                    'errors.entity_not_saved',
                    [
                        'entity' => (new \ReflectionClass($this->getModelClass()))->getShortName(),
                    ]
                ),
                0,
                $model->errors()
            );
        }
        $model = $this->getModelWithEmbedRelations($model, $request);
        $this->afterUpdate($model);

        return RestfulResponse::sendShow($this->getResource($model));
    }

    public function destroy(Request $request, $id)
    {
        $model = $this->getModelClass();
        $query = $model::query();
        $query = $this->columnsFilter($query, $this->params);
        $model = $query->findOrFail($id);

        if ($model->delete($this->request()->all())) {
            $this->afterDelete($model);

            return RestfulResponse::sendDeleted();
        } else {
            throw new AppValidationException(
                __(
                    'errors.entity_not_saved',
                    [
                        'entity' => (new \ReflectionClass($this->getModelClass()))->getShortName(),
                    ]
                ),
                0,
                $model->errors()
            );
        }
    }

    public function afterCreate($object) {}

    public function afterUpdate($object) {}

    public function afterDelete($object) {}
}
