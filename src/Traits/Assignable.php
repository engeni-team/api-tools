<?php

namespace Engeni\ApiTools\Traits;

use Engeni\ApiTools\Context\AppContextRegister;
use Illuminate\Support\Facades\Auth;

trait Assignable
{
    public static function bootAssignable()
    {
        static::creating(function ($model) {
            if ($model->isFillable('created_by') && $user = self::getAssignableUser()) {
                $model->created_by = $user->id;
            }
        });

        static::updating(function ($model) {
            if ($model->isFillable('updated_by') && $user = self::getAssignableUser()) {
                $model->updated_by = $user->id;
            }
        });

        static::deleting(function ($model) {
            if ($model->isFillable('deleted_by') && $user = self::getAssignableUser()) {
                $model->updateQuietly(['deleted_by' => $user->id]);
            }
        });
    }

    public static function getAssignableUser()
    {
        if (app()->bound(AppContextRegister::class) && app_context()->hasUser()) {
            return app_context()->getUser();
        }

        if (Auth::check()) {
            return Auth::user();
        }

        return null;
    }
}
