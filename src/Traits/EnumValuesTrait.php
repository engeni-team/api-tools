<?php

namespace Engeni\ApiTools\Traits;

trait EnumValuesTrait
{
    public static function names(): array
    {
        return array_column(self::cases(), 'name');
    }

    public static function values(): array
    {
        return array_column(self::cases(), 'value');
    }

    public static function array(): array
    {
        return array_combine(self::values(), self::names());
    }

    public static function getCase(mixed $value): ?self
    {
        foreach (self::cases() as $case) {
            if ($case->value === $value) {
                return $case;
            }
        }

        return null;
    }

    public static function getCaseByValue(mixed $key): ?self
    {
        foreach (self::cases() as $case) {
            if ($case->name === $key) {
                return $case;
            }
        }

        return null;
    }
}
