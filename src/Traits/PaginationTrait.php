<?php

namespace Engeni\ApiTools\Traits;

trait PaginationTrait
{
    public int $perPage = 10;

    public int $currentPage = 1;

    public int $totalPages = 0;

    public int $nextPage = 0;

    public int $limitPage = 14;

    /**
     * Load the next page of messages.
     */
    public function nextPage(): void
    {
        $this->currentPage++;
        $this->build();
    }

    /**
     * Load the previous page of messages.
     */
    public function previousPage(): void
    {
        $this->currentPage--;
        $this->build();
    }
}
