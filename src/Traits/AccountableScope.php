<?php

namespace Engeni\ApiTools\Traits;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Scope;

class AccountableScope implements Scope
{
    /**
     * Apply the scope to a given Eloquent query builder.
     *
     * @return void
     */
    public function apply(Builder $builder, Model $model)
    {
        if ($this->shouldBeApplied()) {
            $builder->where("{$model->getTable()}.account_id", app_context()->getUser()->current_account_id);
        }
    }

    protected function shouldBeApplied(): bool
    {
        return app_context()->hasUser() &&
            method_exists(app_context()->getUser(), 'isCxmUser') &&
            ! app_context()->getUser()->isCxmUser() &&
            ! empty(app_context()->getUser()->current_account_id);
    }
}
