<?php

namespace Engeni\ApiTools\Exceptions;

class AppValidationException extends \Exception
{
    public function __construct($message = '', $code = 0, protected array $errors = [])
    {
        $message = empty($message) ? implode(', ', $errors) : $message;
        parent::__construct($message, $code);
    }

    public function getErrors(): array
    {
        return $this->errors;
    }

    // Backwards compatibility
    public function errors(): array
    {
        return $this->errors;
    }
}
