<?php

namespace Engeni\ApiTools\Exceptions;

use Illuminate\Database\Eloquent\Model;

class RecordInvalidException extends AppValidationException
{
    public Model $record;

    public array $errors;

    public function __construct($record)
    {
        // Build a consolidated string of all attribute errors.
        $errors = $record->errors();
        $errorString = '';
        $modelName = class_basename($record);

        foreach ($errors as $attribute => $messages) {
            foreach ($messages as $message) {
                $errorString .= "`$attribute`: $message";
            }
        }

        $this->message = "Validation exception error on {$modelName}: {$errorString}";
        $this->errors = $errors;
        $this->record = $record;
    }
}
