<?php

namespace Engeni\ApiTools\Helpers;

class RestfulResponse
{
    public static $responseHeaders = [];

    public const STATUS_SUCCESS = 'success';

    public const STATUS_ERROR = 'error';

    /**
     * sendEncoded is default Restful API response.
     * For status codes, refer to https://developer.mozilla.org/en-US/docs/Web/HTTP/Status
     *
     * @param  array  $response
     * @return null
     */
    public static function sendEncoded($response, $headers = [])
    {
        $response['code'] ??= 200;
        $headers = array_merge($headers, [
            'X-API-Version' => env('APP_NAME'),
            'Access-Control-Allow-Origin' => '*',
        ]);

        $headers = array_merge($headers, self::$responseHeaders);

        return response()->json(
            $response,
            $response['code'],
            $headers,
            JSON_UNESCAPED_UNICODE
        );
    }

    public static function sendOk(string $message = '')
    {
        return self::sendEncoded(
            array_filter(
                [
                    'code' => 200,
                    'status' => self::STATUS_SUCCESS,
                    'message' => $message,
                ]
            )
        );
    }

    public static function sendShow($data)
    {
        return self::sendEncoded([
            'code' => 200,
            'status' => self::STATUS_SUCCESS,
            'data' => $data,
        ]);
    }

    public static function sendList($data)
    {
        return self::sendShow($data);
    }

    public static function sendCreated($data)
    {
        return self::sendEncoded([
            'code' => 201,
            'status' => self::STATUS_SUCCESS,
            'data' => $data,
        ]);
    }

    public static function sendAccepted()
    {
        return self::sendEncoded([
            'code' => 202,
            'status' => self::STATUS_SUCCESS,
        ]);
    }

    public static function sendDeleted($message = 'Resource deleted.')
    {
        return self::sendEncoded([
            'code' => 202,
            'status' => self::STATUS_SUCCESS,
            'message' => $message,
        ]);
    }

    public static function sendUpdated()
    {
        return self::sendEncoded([
            'code' => 204,
            'status' => self::STATUS_SUCCESS,
        ]);
    }

    public static function sendNotModified($data = [], $message = 'Internal server error.')
    {
        return self::sendEncoded([
            'code' => 304,
            'status' => self::STATUS_SUCCESS,
            'data' => $data,
        ]);
    }

    public static function sendUnauthorized($message = 'Access denied: The request requires user authentication.')
    {
        return self::sendEncoded([
            'code' => 401,
            'status' => self::STATUS_ERROR,
            'message' => $message,
        ]);
    }

    public static function sendForbidden($message = 'Access forbiden: you don’t have permission to access this resource.')
    {
        return self::sendEncoded([
            'code' => 403,
            'status' => self::STATUS_ERROR,
            'message' => $message,
        ]);
    }

    public static function sendNotFound($message = 'Resource Not Found.')
    {
        return self::sendEncoded([
            'code' => 404,
            'status' => self::STATUS_ERROR,
            'message' => $message,
        ]);
    }

    public static function sendClientError($data = [], $message = 'Unprocessable entity.')
    {
        return self::sendEncoded([
            'code' => 422,
            'status' => self::STATUS_ERROR,
            'data' => $data,
            'message' => $message,
        ]);
    }

    public static function sendClientValidationError($e)
    {
        if ($e instanceof ValidationException) {
            $data = ['validation_rules' => $e->getRrrors(), 'exception' => $e->getMessage()];

            return self::sendClientError($data);
        } else {
            $errors = method_exists($e, 'errors') ? $e->errors() : [];

            return self::sendClientError(['errors' => $errors]);
        }
    }

    // public static function sendClientValidationError($e)
    // {
    //     $data = [
    //         'validation_rules' => method_exists($e, 'errors')
    //         ? $e->errors()
    //         : [],
    //         'exception' => $e instanceof \Exception
    //         ? $e->getMessage()
    //         : 'The given data was invalid.',
    //     ];
    //     return self::sendClientError($data);
    // }

    public static function sendServerError($data = [], $message = 'Internal server error.')
    {
        return self::sendEncoded([
            'code' => 500,
            'status' => self::STATUS_ERROR,
            'data' => $data,
            'message' => $message,
        ]);
    }
}
