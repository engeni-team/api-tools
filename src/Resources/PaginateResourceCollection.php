<?php

namespace Engeni\ApiTools\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;

class PaginateResourceCollection extends ResourceCollection
{
    /**
     * Create a new resource instance.
     *
     * @param  mixed  $resource
     * @return void
     */
    public function __construct($paginationResource)
    {
        parent::__construct($paginationResource);
    }

    public function setCollects($resourceClass)
    {
        $this->collects = $resourceClass;
        $this->resource = $this->collectResource($this->resource);
    }

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'data' => $this->collection,

            'current_page' => $this->currentPage(),
            'first_page_url' => $this->url(1),
            'from' => $this->firstItem(),
            'last_page' => $this->lastPage(),
            'last_page_url' => $this->url($this->lastPage()),
            'next_page_url' => $this->nextPageUrl(),
            // 'path' => $this->path, // Can't get $this->path since path is private in the pagination object
            'per_page' => $this->perPage(),
            'prev_page_url' => $this->previousPageUrl(),
            'to' => $this->lastItem(),
            'total' => $this->total(),
        ];
    }
}
