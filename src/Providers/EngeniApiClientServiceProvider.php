<?php

namespace Engeni\ApiTools\Providers;

use Engeni\ApiClient\Client;
use Illuminate\Support\ServiceProvider;

class EngeniApiClientServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     */
    public function register()
    {
        $settings = [
            'base_uri' => config('engeni.api_client.base_uri'),
            'access_token' => config('engeni.api_client.token'),
            'debug' => config('engeni.api_client.debug', false),
            'timeout' => config('engeni.api_client.timeout', 10),
            'headers' => [
                'User-Agent' => config('engeni.service_id'),
                'Accept' => 'application/json',
                'X-Api-Token' => config('engeni.api_client.token'),
            ],
        ];

        if ($bearerToken = request()->bearerToken()) {
            $settings['headers']['Authorization'] = 'Bearer '.$bearerToken;
        }

        $client = new Client($settings);

        $client->setAsGlobal();
    }
}
