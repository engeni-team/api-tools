<table width="100%" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;">
    <tr>
        <td class="info"
            style="word-break:break-word;font-family:'Work Sans', Helvetica, sans-serif;font-size:16px;line-height:24px;padding-top:20px;padding-bottom:20px;padding-right:20px;padding-left:20px;border-radius:4px;background-color:#f4f7fa;">
            <table width="100%" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;">
                @foreach ($items as $key => $value)
                    <tr>
                        <td style="word-break:break-word;font-family:'Work Sans', Helvetica, sans-serif;font-size:16px;line-height:24px;">
                            <strong style="font-weight:600;">{{ $key }}:</strong> {!! $value !!}
                        </td>
                    </tr>
                @endforeach
            </table>
        </td>
    </tr>
</table>
