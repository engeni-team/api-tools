<table width="100%" align="center" cellpadding="0" cellspacing="0" role="presentation"
       style="border-collapse:collapse; border: 2px solid #f4f7fa; margin-bottom: 20px;">
    <tr>
        <td align="center"
            style="padding-top:10px;padding-bottom:10px;padding-right:0;padding-left:0;word-break:break-word;font-family:'Inter', Helvetica, Arial, sans-serif;font-size:16px;line-height:24px;">
            <table width="100%" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;">
                <tr>
                    <td class="info info-lg" align="left"
                        style="word-break:break-word;font-family:'Inter', Helvetica, sans-serif;font-size:16px;line-height:24px;border-radius:4px;background-color:#FFF;padding-top:20px;padding-bottom:10px;padding-right:10px;padding-left:10px;">
                        <p style="margin-top:0;color:#111111;font-size:20px;line-height:36px;font-weight:600;margin-bottom:24px; text-align: center">{{ $title }}</p>
                        {{ $slot ?? '' }}
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
