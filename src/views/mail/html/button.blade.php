@props([
    'url',
    'color' => 'primary',
    'align' => 'center',
])
<table width="100%" align="center" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;">
    <tr>
        <td align="center"
            style="padding-top:20px;padding-bottom:20px;padding-right:0;padding-left:0;word-break:break-word;font-family:'Work Sans', Helvetica, sans-serif;font-size:16px;line-height:24px;">
            <table class="mobile-wide" border="0" cellspacing="0" cellpadding="0" role="presentation" style="border-collapse:collapse;">
                <tr>
                    <td align="center" class="btn"
                        style="word-break:break-word;font-family:'Work Sans', Helvetica, sans-serif;font-size:16px;line-height:24px;background-color:#1e40af;box-shadow:0 4px 6px -1px rgba(0,0,0,.1), 0 2px 4px -1px rgba(0,0,0,.06);border-radius:3px;">
                        <a href="{{ $url }}" target="_blank"
                           style="background-color:#1e40af;padding-top:14px;padding-bottom:14px;padding-right:30px;padding-left:30px;display:inline-block;color:#FFF;text-decoration:none;border-radius:3px;-webkit-text-size-adjust:none;box-sizing:border-box;border-width:0px;border-style:solid;border-color:#1e40af;font-weight:600;font-size:15px;line-height:21px;letter-spacing:0.25px;">
                            {{ $slot }}
                        </a>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
</table>
