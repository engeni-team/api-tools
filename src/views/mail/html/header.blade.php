@if (trim($slot) === '')
    <table role="presentation" cellpadding="0" cellspacing="0" border="0" align="center" style="width: 100%; min-width: 100%;" width="100%">
        <tr>
            <td height="40" style="line-height: 20px; min-height: 20px;">
            </td>
        </tr>
    </table>
    <p class="logo"
       style="margin-right:0;margin-left:0;padding-bottom: 20px; padding-top: 0px; height: 30px;color:#111111;text-align:left;margin-top:0;margin-bottom:0px">
    </p>
    <table width="100%" style="border-collapse:collapse;">
        <tr>
            <td height="20" style="font-size:0px;line-height:0px;word-break:break-word;font-family:'Work Sans', Helvetica, sans-serif;">
                &nbsp;
            </td>
        </tr>
    </table>
@else
    {{ $slot }}
@endif
