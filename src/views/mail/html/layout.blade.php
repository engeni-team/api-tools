<!DOCTYPE html>
<html>

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title></title>

    <style type="text/css" rel="stylesheet" media="all">
        @media only screen and (max-width: 640px) {

            .ms-header {
                display: none !important;
            }

            .ms-content {
                width: 100% !important;
                border-radius: 0;
            }

            .ms-content-body {
                padding: 30px !important;
            }

            .ms-footer {
                width: 100% !important;
            }

            .mobile-wide {
                width: 100% !important;
            }

            .info-lg {
                padding: 30px;
            }
        }
    </style>
    <!--[if mso]>
    <style type="text/css">
        body {
            font-family: 'Work Sans', Helvetica, Arial, sans-serif !important;
        }

        td {
            font-family: 'Work Sans', Helvetica, Arial, sans-serif !important;
        }

        td * {
            font-family: 'Work Sans', Helvetica, Arial, sans-serif !important;
        }

        td p {
            font-family: 'Work Sans', Helvetica, Arial, sans-serif !important;
        }

        td a {
            font-family: 'Work Sans', Helvetica, Arial, sans-serif !important;
        }

        td span {
            font-family: 'Work Sans', Helvetica, Arial, sans-serif !important;
        }

        td div {
            font-family: 'Work Sans', Helvetica, Arial, sans-serif !important;
        }

        td ul li {
            font-family: 'Work Sans', Helvetica, Arial, sans-serif !important;
        }

        td ol li {
            font-family: 'Work Sans', Helvetica, Arial, sans-serif !important;
        }

        td blockquote {
            font-family: 'Work Sans', Helvetica, Arial, sans-serif !important;
        }

        th * {
            font-family: 'Work Sans', Helvetica, Arial, sans-serif !important;
        }
    </style>
    <![endif]-->
</head>

<body
    style="font-family:'Work Sans', Helvetica, sans-serif; width: 100% !important; height: 100%; margin: 0; padding: 0; -webkit-text-size-adjust: none; background-color: #f4f7fa; color: #4a5566;">

<div class="preheader"
     style="display:none !important;visibility:hidden;mso-hide:all;font-size:1px;line-height:1px;max-height:0;max-width:0;opacity:0;overflow:hidden;">
</div>

<table class="ms-body" width="100%" cellpadding="0" cellspacing="0" role="presentation"
       style="border-collapse:collapse;background-color:#f4f7fa;width:100%;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
    <tr>
        <td align="center"
            style="word-break:break-word;font-family:'Work Sans', Helvetica, sans-serif;font-size:16px;line-height:24px;">

            <table class="ms-container" width="100%" cellpadding="0" cellspacing="0"
                   style="border-collapse:collapse;width:100%;margin-top:0;margin-bottom:0;margin-right:0;margin-left:0;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;">
                <tr>
                    <td align="center"
                        style="word-break:break-word;font-family:'Work Sans', Helvetica, sans-serif;font-size:16px;line-height:24px;">
                        {{ $header ?? '' }}
                    </td>
                </tr>
                <!-- Email Body -->
                <tr>
                    <td align="center"
                        style="word-break:break-word;font-family:'Work Sans', Helvetica, sans-serif;font-size:16px;line-height:24px;">

                        <table class="ms-content" width="640" cellpadding="0" cellspacing="0" role="presentation"
                               style="border-collapse:collapse;width:640px;margin-top:0;margin-bottom:0;margin-right:auto;margin-left:auto;padding-top:0;padding-bottom:0;padding-right:0;padding-left:0;background-color:#FFFFFF;border-radius:6px;box-shadow:0 3px 6px 0 rgba(0,0,0,.05);">
                            <tr>
                                <td class="ms-content-body"
                                    style="word-break:break-word;font-family:'Work Sans', Helvetica, sans-serif;font-size:16px;line-height:24px;padding-top:0px;padding-right:50px;padding-left:50px;">

                                    <table role="presentation" cellpadding="0" cellspacing="0" border="0"
                                           align="center"
                                           style="width: 100%; min-width: 100%;" width="100%">
                                        <tr>
                                            <td height="40" style="line-height: 20px; min-height: 20px;">
                                            </td>
                                        </tr>
                                    </table>

                                    <p class="logo"
                                       style="margin-right:0;margin-left:0;padding-bottom: 20px; padding-top: 0px; height: 30px;color:#111111;text-align:left;margin-top:0;margin-bottom:0px">
                                        <img src="https://static.engeni.com/emails/logos/logo-trimmed.png"
                                             width="100"
                                             style="margin:0; padding:0; border:none; display:block;" border="0"
                                             alt="Engeni International LLC"/>
                                    </p>

                                    <table width="100%" style="border-collapse:collapse;">
                                        <tr>
                                            <td height="20"
                                                style="font-size:0px;line-height:0px;word-break:break-word;font-family:'Work Sans', Helvetica, sans-serif;">
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="20"
                                                style="font-size:0px;line-height:0px;border-top-width:1px;border-top-style:solid;border-top-color:#e2e8f0;word-break:break-word;font-family:'Work Sans', Helvetica, sans-serif;">
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>

                                    <!-- CONTENT STARTS HERE -->
                                    {{ Illuminate\Mail\Markdown::parse($slot) }}

                                    {{ $subcopy ?? '' }}
                                    <!-- CONTENT ENDS HERE -->

                                    <table role="presentation" cellpadding="0" cellspacing="0" border="0"
                                           align="center"
                                           style="width: 100%; min-width: 100%;" width="100%">
                                        <tr>
                                            <td height="40" style="line-height: 20px; min-height: 20px;">
                                            </td>
                                        </tr>
                                    </table>

                                </td>
                            </tr>
                        </table>

                    </td>
                </tr>

                {{ $footer ?? '' }}
            </table>
        </td>
    </tr>
</table>
</body>
</html>
