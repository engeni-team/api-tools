<tr>
    <td align="center"
        style="word-break:break-word;font-family:'Work Sans', Helvetica, sans-serif;font-size:16px;line-height:24px;">

        <table class="ms-footer" width="640" cellpadding="0" cellspacing="0" role="presentation"
               style="border-collapse:collapse;width:640px;margin-top:0;margin-bottom:0;margin-right:auto;margin-left:auto;">
            <tr>
                <td class="ms-content-body" align="center"
                    style="word-break:break-word;font-family:'Work Sans', Helvetica, sans-serif;font-size:16px;line-height:24px;padding-top:40px;padding-bottom:0px;padding-right:50px;padding-left:50px;">
                    <img src="https://static.engeni.com/emails/logos/logo-trimmed.png" width="80"
                         style="margin:0; padding:0; border:none; display:block;" border="0"
                         alt="Engeni International LLC"/>
                </td>
            </tr>
        </table>
        <table class="ms-footer" width="640" cellpadding="0" cellspacing="0" role="presentation"
               style="border-collapse:collapse;width:640px;margin-top:0;margin-bottom:0;margin-right:auto;margin-left:auto;">
            <tr>
                <td class="ms-content-body small" align="center"
                    style="color:#96a2b3;word-break:break-word;font-family:'Work Sans', Helvetica, sans-serif;font-size:12px;line-height:22px;padding-top:20px;padding-bottom:20px;padding-right:50px;padding-left:50px;">
                    © {{ date('Y') }} Engeni International LLC. All rights reserved.
                </td>
            </tr>
        </table>

    </td>
</tr>
