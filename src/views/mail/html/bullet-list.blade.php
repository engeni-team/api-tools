<table width="100%" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;">
<tr>
<td class="info"
style="word-break:break-word;font-family:'Work Sans', Helvetica, sans-serif;font-size:16px;line-height:24px;padding-top:20px;padding-bottom:20px;padding-right:20px;padding-left:20px;border-radius:4px;background-color:#f4f7fa;">
<table width="100%" cellpadding="0" cellspacing="0" role="presentation" style="border-collapse:collapse;">
@foreach ($items as $key => $value)
@if(isset($value))
<tr>
<td style="word-break:break-word;font-family:'Work Sans', Helvetica, sans-serif;font-size:16px;line-height:24px; padding: 0; text-align: left;">
• {!! strip_tags(Illuminate\Mail\Markdown::parse($value), '<a>') !!}
</td>
</tr>
@endif
@endforeach
</table>
</td>
</tr>
</table>
