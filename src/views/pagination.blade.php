@if($totalPages > $perPage)
    <tfoot>
        <tr>
            <td colspan="8">
                <div class="d-flex justify-content-between">
                    <nav aria-label="Page navigation" class="ps-3">
                        <ul class="pagination">
                            <li class="page-item {{ $currentPage == 1 ? 'disabled' : '' }}">
                                <a class="page-link" href="#" wire:click.prevent="previousPage" aria-label="Previous">
                                    <span aria-hidden="true">&laquo;</span>
                                </a>
                            </li>
                            @php
                                $start = max(1, $currentPage - 6);
                                $end = min($start + $limitPage, ceil($totalPages / $perPage));
                                $start = max(1, $end - $limitPage);
                            @endphp
                            @if ($start > 1)
                                <li class="page-item">
                                    <a class="page-link" href="#" wire:click.prevent="build(1)">1</a>
                                </li>
                                @if ($start > 2)
                                    <li class="page-item disabled"><span class="page-link">...</span></li>
                                @endif
                            @endif
                            @for ($i = $start; $i <= $end; $i++)
                                <li class="page-item {{ $currentPage == $i ? 'active' : '' }}">
                                    <a class="page-link" href="#" wire:click.prevent="build({{ $i }})">{{ $i }}</a>
                                </li>
                            @endfor
                            @if ($end < ceil($totalPages / $perPage))
                                @if ($end < ceil($totalPages / $perPage) - 1)
                                    <li class="page-item disabled"><span class="page-link">...</span></li>
                                @endif
                                <li class="page-item">
                                    <a class="page-link" href="#" wire:click.prevent="build({{ ceil($totalPages / $perPage) }})">{{ ceil($totalPages / $perPage) }}</a>
                                </li>
                            @endif
                            <li class="page-item {{ $currentPage == ceil($totalPages / $perPage) ? 'disabled' : '' }}">
                                <a class="page-link" href="#" wire:click.prevent="nextPage" aria-label="Next">
                                    <span aria-hidden="true">&raquo;</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                    <div class="pt-3 pe-3">
                        <span><strong>Current:</strong> {{ $currentPage }} / {{ ceil($totalPages / $perPage) }} pages. <strong>Total:</strong> {{ $totalPages }}</span>
                    </div>
                </div>
            </td>
        </tr>
    </tfoot>
@endif

<script>
    document.addEventListener('DOMContentLoaded', function () {
        document.querySelectorAll('.page-link').forEach(function (element) {
            element.addEventListener('click', function () {
                this.blur();
            });
        });
    });
</script>
