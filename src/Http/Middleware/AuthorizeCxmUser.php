<?php

namespace Engeni\ApiTools\Http\Middleware;

use Closure;
use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Http\Request;

class AuthorizeCxmUser extends Middleware
{
    /**
     * Handle an incoming request.
     *
     * @param  Request  $request
     * @param  mixed  ...$guards
     */
    public function handle($request, Closure $next, ...$guards): mixed
    {
        if (! app_context()->hasUser() || ! app_context()->getUser()->isCxmUser()) {
            return response()->json([
                'message' => 'Unauthorized',
            ], 403);
        }

        return $next($request);
    }
}
