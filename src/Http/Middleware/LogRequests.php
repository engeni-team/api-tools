<?php

namespace Engeni\ApiTools\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Log;

class LogRequests
{
    public function handle($request, Closure $next)
    {
        if (config('engeni.logs.requests', false)) {
            $headers = [];
            foreach ($request->header() as $header => $value) {
                if ($value[0]) {
                    $headers[$header] = $value[0];
                }
            }

            Log::channel(config('engeni.logs.requests_channel', 'requests'))->info(
                [
                    'url' => $request->fullUrl(),
                    'method' => $request->method(),
                    'headers' => $headers,
                    'content' => $request->all(),
                ]
            );
        }

        return $next($request);
    }
}
